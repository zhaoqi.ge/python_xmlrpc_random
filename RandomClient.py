
import sys

import xmlrpc.client

random = xmlrpc.client.ServerProxy( 'http://localhost:8000' )

if __name__ == '__main__':
  if len( sys.argv ) > 2:
    random.setBounds( int( sys.argv[1] ), int( sys.argv[2] ))
  else:
    for i in range( 10 ):
      print( random.nextRandom(), "", end='' )
    print()
